export default (editor, opts = {}) => {
  const dc = editor.DomComponents;
  const defaultType = dc.getType('default');
  const defaultView = defaultType.view;

  const componentName = 'playbook-button'
  const styleName = componentName;

  const traitManager = editor.TraitManager;
  const textTrait = traitManager.getType('text');

  const cssc = editor.CssComposer;
  const createCssStyles = () => {
    const css = `
      .${styleName} {
        display: inline-block;
        cursor: pointer;
        font-weight: 400;
        color: #212529;
        text-align: center;
        vertical-align: middle;

        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        
        background-color: transparent;
        border: 1px solid transparent;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        border-radius: .25rem;
        transition: color .15s ease-in-out,background-color 
                          .15s ease-in-out,border-color
                          .15s ease-in-out,box-shadow
                          .15s ease-in-out;
      }

      .${styleName}--primary {
        color: #fff;
        background-color: #007bff;
        border-color: #007bff;
      }

      .${styleName}--secondary {
        color: #fff;
        background-color: #6c757d;
        border-color: #6c757d;
      }

      .${styleName}--success {
        color: #fff;
        background-color: #28a745;
        border-color: #28a745;
      }

      .${styleName}--danger {
        color: #fff;
        background-color: #dc3545;
        border-color: #dc3545;
      }

      .${styleName}--dark {
        color: #fff;
        background-color: #343a40;
        border-color: #343a40;
      }
    `
    cssc.getAll().add(css);
  }

  editor.TraitManager.addType('content', {
    events:{
      'keyup': 'onChange',
    },

    onValueChange: function () {
      const md = this.model;
      const target = md.target;
      target.set('content', md.get('value'));
    },

    getInputEl: function() {
      if(!this.inputEl) {
        this.inputEl = textTrait.prototype.getInputEl.bind(this)();
        this.inputEl.value = this.target.get('content');
      }
      return this.inputEl;
    }
  });

  dc.addType(`${componentName}`, {
    isComponent: function (el) {
      if (el.tagName && el.tagName.toLowerCase() === 'div'
        && el.hasAttribute && el.hasAttribute('data-component-marker')) {
        console.log('Detecting playbook button component.');
        if (el.getAttribute('data-component-marker') === `${componentName}`) {
          console.log('Playbook button component detected ...');
          return { type: `${componentName}` };
        }
      }
    },
    model: {
      defaults: {
        name: 'Button',
        content: 'This is a button',
        attributes: {
          'data-component-marker': `${componentName}`,
        },
        tagName: 'button',
        classes: `${styleName}`,
        traits: [{
          type: 'content',
          label: 'Text',
        }, {
          label: 'Button Type',
          type: 'select',
          name: 'type',
          options: [
            { value: 'submit', name: 'Submit' },
            { value: 'reset', name: 'Reset' },
            { value: 'button', name: 'Button' },
          ]
        }]
      },
    },
    view: defaultView.extend({
      events: {
        'click': 'handleClick'
      },

      init() {
        createCssStyles();
        this.listenTo(this.model, 'change:content', this.updateContent);
      },

      updateContent() {
        this.el.innerHTML = this.model.get('content')
      },

      handleClick(e) {
        e.preventDefault();
      },
    }),
  });
}
